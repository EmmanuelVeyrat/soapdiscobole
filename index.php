<?php
require_once "nusoap-0.9.5/lib/nusoap.php";
$path = realpath(dirname(__FILE__)) . '/csv/';

$wsdl = "http://82.127.159.82:8080/LCVMAGWS_WEB/awws/LcvMagWS.awws?wsdl";

$method = "LectureDesModeles";

$login = "DISCO";
$password = "BOLE";

$client = new nusoap_client($wsdl, true);

$error = $client->getError();
if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

$result = $client->call($method, array(
    'bCData'      => false,
	'sUser'      => $login,
	'sMdp'       => $password,
	));

if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
}
else {
    $error = $client->getError();

    if ($error) {
        echo "<h2>Error</h2><pre>" . $error . "</pre>";
    }
    else {
        if(count($result['Resultat']['Modele'])){
            $fp=fopen($path . date('Y-m-d') . '-' . md5(time()) . '-csv','w');
            $row = "sku;type;price;simples_skus;qty;is_in_stock;delivery_date;name;use_config_backorders;tax_class_id;store;websites;status";
            fputcsv($fp,split(';',$row),';');
            foreach ($result['Resultat']['Modele'] as $key => $value) {
                $sku = $value['NomFour'];
                $type = $value['NomFour'];
                $price = $value['NomFour'];
                $simples_skus = $value['NomFour'];
                $qty = $value['NomFour'];
                $is_in_stock = $value['NomFour'];
                $delivery_date = '1899-12-31';
                $name = $value['NomFour'];
                $use_config_backorders = '0';
                $tax_class_id = 'TVA 19.6';
                $store = 'admin,default,newrock';
                $websites = 'base,newrock';
                $status = $value['NomFour'];

                $row = "$sku;$type;$price;$simples_skus;$qty;$is_in_stock;$delivery_date;$name;$use_config_backorders;$tax_class_id;$store;$websites;$status";
                fputcsv($fp,split(';',$value),';');
            }
            fclose($fp);
        }
    }
}